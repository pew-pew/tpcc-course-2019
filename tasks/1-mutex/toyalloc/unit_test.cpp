#include "toyalloc.hpp"

#include <twist/memory/mmap_allocation.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <algorithm>

static const size_t kPageSize = 4096;

TEST_SUITE(PageAlloc) {
  SIMPLE_TEST(AllocateThenFree) {
    void* addr = toyalloc::Allocate();
    ASSERT_TRUE(addr != nullptr);
    toyalloc::Free(addr);
  }

  SIMPLE_TEST(AllocateAllArenaTwice) {
    auto arena = toyalloc::GetArena();

    size_t page_count = arena.Size() / kPageSize;

    std::vector<void*> allocated;
    for (size_t i = 0; i < page_count; ++i) {
      void* addr = toyalloc::Allocate();
      ASSERT_TRUE(addr != nullptr);
      allocated.push_back(addr);
    }

    // Arena exhausted
    ASSERT_EQ(toyalloc::Allocate(), nullptr);

    // Free and allocate again
    void* first = allocated[0];
    toyalloc::Free(first);
    allocated[0] = toyalloc::Allocate();
    ASSERT_EQ(allocated[0], first);

    // Release all
    for (size_t i = 0; i < allocated.size(); ++i) {
      toyalloc::Free(allocated[i]);
    }

    // Allocate all again
    for (size_t i = 0; i < allocated.size(); ++i) {
      allocated[i] = toyalloc::Allocate();
    }

    std::sort(allocated.begin(), allocated.end());

    for (size_t i = 0; i < allocated.size(); ++i) {
      char* start = arena.Begin() + i * kPageSize;
      ASSERT_EQ(start, allocated[i]);
    }
  }
}

void InitAllocator() {
  static const size_t kArenaPages = 1024;
  auto arena = twist::MmapAllocation::AllocatePages(kArenaPages);
  toyalloc::Init(std::move(arena));
}

int main() {
  InitAllocator();
  RunTests(ListAllTests());
}
