#pragma once

// Fast user-space locking
// http://man7.org/linux/man-pages/man2/futex.2.html

// Tests that the value at the futex word pointed to by address 'addr'
// still contains the expected value 'value' and if so, then sleeps
// waiting for a FutexWake operation on the futex word

int FutexWait(unsigned int* addr, int value);


// Wakes at most 'count' of the waiters that are waiting on the futex
// word at the address 'addr'

int FutexWake(unsigned int* addr, int count);

