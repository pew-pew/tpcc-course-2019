#include "futex.hpp"

#include <twist/thread/native_futex.hpp>

#if defined(TWIST_FAULTY)

#include <twist/fault/inject_fault.hpp>

int FutexWait(unsigned int* addr, int value) {
  twist::fault::InjectFault();
  return twist::thread::FutexWait(addr, value);
  twist::fault::InjectFault();
}

int FutexWake(unsigned int* addr, int count) {
  twist::fault::InjectFault();
  return twist::thread::FutexWake(addr, count);
  twist::fault::InjectFault();
}

#else

int FutexWait(unsigned int* addr, int value) {
  return twist::thread::FutexWait(addr, value);
}

int FutexWake(unsigned int* addr, int count) {
  return twist::thread::FutexWake(addr, count);
}

#endif
